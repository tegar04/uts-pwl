<?php

use app\modules\master\models\Agama;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\master\models\Pegawai */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pegawai-form">

    <?php $form = ActiveForm::begin();?>

    <?= $form->field($model, 'nip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'gender')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'id_agama')->widget(Select2::classname(), [
        'data' => $agama,
        'options' => ['placeholder' => 'Search for a Agama ...'],
    ]);
    ?>

    <?= $form->field($model, 'id_divisi')->widget(Select2::classname(), [
        'data' => $divisi,
        'options' => ['placeholder' => 'Search for a Divisi ...'],
    ]); ?>

    <?= $form->field($model, 'id_jabatan')->widget(Select2::classname(), [
        'data' => $jabatan,
        'options' => ['placeholder' => 'Search for a Jabatan ...'],
    ]); ?>
    <?= $form->field($model, 'id_province')->dropDownList($province, ['id' => 'id_province']) ?>

    <?= $form->field($model, 'id_city')->dropDownList($city, ['id' => 'id_city', 'disabled' => 'disabled']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?=
    $this->registerJsFile(
        '@web/js/city.js',
        ['depends' => [\yii\web\JqueryAsset::className()]]
    );
?>
